@extends('core::layouts.master')
@section('title')
    {{ trans_choice('core::general.add',1) }} {{ trans_choice('loan::general.charge',1) }}
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        {{ trans_choice('core::general.add',1) }} {{ trans_choice('loan::general.charge',1) }}
                        <a href="#" onclick="window.history.back()"
                           class="btn btn-outline-light bg-white d-none d-sm-inline-flex">
                            <em class="icon ni ni-arrow-left"></em><span>{{ trans_choice('core::general.back',1) }}</span>
                        </a>
                    </h1>

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a
                                    href="{{url('dashboard')}}">{{ trans_choice('dashboard::general.dashboard',1) }}</a>
                        </li>
                        <li class="breadcrumb-item"><a
                                    href="{{url('loan/'.$loan->id.'/show')}}">{{ trans_choice('loan::general.loan',2) }}</a>
                        </li>
                        <li class="breadcrumb-item active">{{ trans_choice('core::general.add',1) }} {{ trans_choice('loan::general.charge',1) }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content" id="app">
        <form method="post" action="{{ url('loan/'.$loan->id.'/charge/store') }}">
            {{csrf_field()}}
            <div class="card card-bordered card-preview">
                <div class="card-body">
                    <div class="form-group">
                        <label for="loan_charge_id"
                               class="control-label">{{trans_choice('loan::general.charge',1)}}</label>
                        <select class="form-control  @error('loan_charge_id') is-invalid @enderror" name="loan_charge_id" id="loan_charge_id"
                                v-model="loan_charge_id" v-on:click="change_charge" required>
                            <option value=""></option>
                            <option v-for="(charge,index) in charges" v-bind:value="index">
                                @{{ charge.name }}
                            </option>
                        </select>
                        @error('loan_charge_id')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="amount" class="control-label">{{trans('core::general.amount')}}</label>
                        <input type="text" name="amount" value="{{ old('amount') }}" id="amount" v-model="amount" v-if="chargeIsSelected == false"
                               class="form-control  @error('amount') is-invalid @enderror numeric" required>
                        <input type="text" name="amount" value="{{ old('amount') }}" id="amount" v-model="amount" v-if="chargeIsSelected == true && loan_charge_id != 4"
                               class="form-control  @error('amount') is-invalid @enderror numeric" required>      
                        <input type="text" name="amount" value="welewele" v-bind:value="creditLife" id="amount" v-if="chargeIsSelected == true && loan_charge_id == 4"
                               class="form-control  @error('amount') is-invalid @enderror numeric" required>
                        @error('amount')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="loan_charge_type_id"
                            class="control-label">Is Paid</label>
                        <select class="form-control select2" name="is_paid" id="is_paid" required>
                            <option value=""></option>
                            <option value="1">Paid Up</option>
                            <option value="0">Unpaid</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="date" class="control-label">{{trans('core::general.date')}}</label>
                        <flat-pickr
                                v-model="date"
                                class="form-control  @error('date') is-invalid @enderror"
                                name="date" id="date" required>
                        </flat-pickr>
                        @error('date')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer border-top ">
                    <button type="submit"
                            class="btn btn-primary  float-right">{{trans_choice('core::general.save',1)}}</button>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('scripts')
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                loan_charge_id: "{{ old('loan_charge_id') }}",
                amount: "{{ old('amount') }}",
                date: "{{ old('date',date("Y-m-d")) }}",
                loan_amount: parseFloat("{{$loan->applied_amount}}"),
                period: parseFloat("{{$loan->loan_term}}"),
                charges: charges,
                chargeIsSelected : false
            },
            computed : {
                creditLife() {
                    //console.log("{{$loan->loan_term}}")
                    //console.log("{{$loan->applied_amount}}")
                    let amt = ((5.03* this.period) + 3.03) * this.loan_amount/6000
                    return amt
                }
            },
            methods: {
                change_charge() {
                    this.amount = charges[this.loan_charge_id].amount;
                    if(this.loan_charge_id == ''){
                        this.chargeIsSelected = false
                    }else{
                        this.chargeIsSelected = true
                    }
                },
                onSubmit() {

                }
            }
        })
    </script>
@endsection